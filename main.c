#include <stdio.h>
#include <string.h>
#include <time.h>
#include "http.h"


/*
 * If using Cmake, use set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pthread -lcurl") to enable CURL
 * You can also take a look at: https://github.com/json-c/json-c
 * */

#define BUF_LEN 256

int main() {

    for(int i = 1; i < 100; ++i){
        char buf[BUF_LEN] = {0};
        time_t rawtime = time(NULL);
        if (rawtime == -1) {

            puts("The time() function failed");
            return 1;
        }

        struct tm *ptm = localtime(&rawtime);
        if (ptm == NULL) {

            puts("The localtime() function failed");
            return 1;
        }

        strftime(buf, BUF_LEN, "%FT%T%z", ptm);

        char payload[4096];
        snprintf(
                payload,
                4096,
                "[{\"measurement\":\"cpu\",\"tags\":{\"host_name\":\"example\",\"miner_id\":\"super_miner\"},\"time\":\"%s\",\"captures\":{\"value\":%f}}]",
                buf,
                (float)i/100.0
                );

        printf("Generated Payload: %s \n", payload);
        int result = send_http_post(
                "/cpu_metric",
                payload,
                "application/json"
        );
        printf("\n CODE: %d \n", result);
        printf("Sent! \n");
    }


    return 0;
}


